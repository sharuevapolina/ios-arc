// Описание структур легкового автомобиля и грузовика

struct Car: Hashable {
    let make: String
    let year: Int
    let trunkVolume: Int
    var engineIsOn: Bool
    var windowsAreOpen: Bool
    var trunkFilledVolume: Int

    mutating func performAction(_ action: CarAction) {
        switch action {
        case .startEngine:
            engineIsOn = true
        case .stopEngine:
            engineIsOn = false
        case .openWindows:
            windowsAreOpen = true
        case .closeWindows:
            windowsAreOpen = false
        case .loadTrunk(let volume):
            trunkFilledVolume += volume
        case .unloadTrunk(let volume):
            trunkFilledVolume -= volume
        }
    }
}

struct Truck: Hashable {
    let make: String
    let year: Int
    let bodyVolume: Int
    var engineIsOn: Bool
    var windowsAreOpen: Bool
    var bodyFilledVolume: Int

    mutating func performAction(_ action: TruckAction) {
        switch action {
        case .startEngine:
            engineIsOn = true
        case .stopEngine:
            engineIsOn = false
        case .openWindows:
            windowsAreOpen = true
        case .closeWindows:
            windowsAreOpen = false
        case .loadBody(let volume):
            bodyFilledVolume += volume
        case .unloadBody(let volume):
            bodyFilledVolume -= volume
        }
    }
}

// Описание перечисления с возможными действиями с автомобилем

enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadTrunk(Int)
    case unloadTrunk(Int)
}

enum TruckAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadBody(Int)
    case unloadBody(Int)
}

// Инициализация экземпляров структур

var car1 = Car(make: "Toyota", year: 2020, trunkVolume: 500, engineIsOn: false, windowsAreOpen: false, trunkFilledVolume: 0)
var car2 = Car(make: "Honda", year: 2021, trunkVolume: 400, engineIsOn: true, windowsAreOpen: true, trunkFilledVolume: 200)
var truck1 = Truck(make: "Ford", year: 2019, bodyVolume: 1000, engineIsOn: false, windowsAreOpen: false, bodyFilledVolume: 0)
var truck2 = Truck(make: "Chevrolet", year: 2022, bodyVolume: 1500, engineIsOn: true, windowsAreOpen: true, bodyFilledVolume: 500)

// Применение различных действий к экземплярам структур

car1.performAction(.startEngine)
car1.performAction(.openWindows)
car1.performAction(.loadTrunk(100))

car2.performAction(.stopEngine)
car2.performAction(.closeWindows)
car2.performAction(.unloadTrunk(50))

truck1.performAction(.startEngine)
truck1.performAction(.openWindows)
truck1.performAction(.loadBody(200))

truck2.performAction(.stopEngine)
truck2.performAction(.closeWindows)
truck2.performAction(.unloadBody(100))

// Помещение объектов структур в словарь

var dict: [AnyHashable: String] = [:]
dict[car1] = "car1"
dict[car2] = "car2"
dict[truck1] = "truck1"
dict[truck2] = "truck2"

// Вывод словаря на печать

print(dict)

