class Man{
    var passport: Passport?

    deinit {
        // выведем в консоль сообщение о том, что объект удален
        print("мужчина удален из памяти")
    }
}

class Passport{
    unowned let man: Man

    init(man: Man) {
        self.man = man
    }

    deinit {
        // выведем в консоль сообщение о том, что объект удален
        print("паспорт удален из памяти")
    }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)
man?.passport = passport
passport = nil // объект еще не удален, его удерживает мужчина
man = nil // теперь удален только мужчина, паспорт остался в памяти

// Чтобы удалить паспорт из памяти, нужно разорвать циклическую ссылку
man?.passport = nil
